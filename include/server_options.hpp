/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SERVER_OPTIONS_HPP
#define SERVER_OPTIONS_HPP
#include <cstdint> // for uint16_t
#include <string>  // for string

namespace tftp {
class server_options {
  public:
    static const bool default_allowed_to_read = true;
    static const bool default_allowed_to_write = false;
    static const bool default_allowed_to_create_subdirs = false;
    static const std::uint16_t default_port = 69;

    bool is_allowed_to_read() const;
    bool is_allowed_to_write() const;
    bool is_allowed_to_create_subdirs() const;
    std::uint16_t get_port() const;
    std::string get_root_path() const;

  private:
    server_options(bool allowed_to_read, bool allowed_to_write, bool allowed_to_create_subdirs, std::uint16_t port,
                   std::string root_path);
    void check_write_and_catalog_perms();

    bool allowed_to_read_;
    bool allowed_to_write_;
    bool allowed_to_create_subdirs_;
    std::uint16_t port_;
    std::string root_path_;
    friend class server_options_builder;
};
} // namespace tftp
#endif // SERVER_OPTIONS_HPP

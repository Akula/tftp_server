/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SERVER_OPTIONS_BUILDER_HPP
#define SERVER_OPTIONS_BUILDER_HPP
#include <cstdint> // for uint16_t
#include <string>  // for string

namespace tftp {
class server_options;
class server_options_builder {
  public:
    server_options_builder();
    server_options_builder& allowed_to_read(bool allowed);
    server_options_builder& allowed_to_write(bool allowed);
    server_options_builder& allowed_to_create_subdirs(bool allowed);
    server_options_builder& set_root_path(std::string path);
    server_options_builder& set_port(std::uint16_t port);
    server_options build();

  private:
    bool allowed_to_read_;
    bool allowed_to_write_;
    bool allowed_to_create_subdirs_;
    std::uint16_t port_;
    std::string root_path_;
};
} // namespace tftp
#endif // SERVER_OPTIONS_BUILDER_HPP

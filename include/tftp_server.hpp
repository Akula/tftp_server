/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef TFTP_SERVER_HPP
#define TFTP_SERVER_HPP
#include <memory> //for unique_ptr

namespace tftp {
class server_options;

class tftp_server {
  public:
    tftp_server(server_options options);

    ~tftp_server();
    tftp_server(tftp_server&& server) noexcept;
    tftp_server& operator=(tftp_server&& server) noexcept;
    tftp_server(const tftp_server& server) = delete;
    tftp_server& operator=(const tftp_server& server) = delete;
    void start_server();

  private:
    class tftp_server_impl;
    std::unique_ptr<tftp_server_impl> pimpl_;
};
} // namespace tftp

#endif // TFTP_SERVER_HPP

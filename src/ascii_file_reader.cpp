/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ascii_file_reader.hpp"
#include <array>                              // for array
#include <boost/filesystem/fstream.hpp>       // for ifstream
#include <boost/iostreams/detail/error.hpp>   // for bad_putback
#include <boost/iostreams/detail/forward.hpp> // for stream_buffer::open
#include <boost/iostreams/filter/regex.hpp>   // for basic_regex_filter
#include <boost/regex.hpp>                    // for regex
#include <cstddef>                            // for size_t
#include <gsl/gsl_util>                       // for narrow
#include <ios>                                // for streamsize
#include <istream>                            // for size_t, basic_istream:...

namespace tftp {
ascii_file_reader::ascii_file_reader(const boost::filesystem::path& path) : file_reader{path} {
    prepare_filtering_stream();
}

void ascii_file_reader::read_next_chunk_impl() {
    fistream_.read(now_chunk_.data(), gsl::narrow<std::streamsize>(chunk_size_));
}
std::size_t ascii_file_reader::get_bytes_read_impl() { return gsl::narrow<std::size_t>(fistream_.gcount()); }

void ascii_file_reader::prepare_filtering_stream() {
    using boost::iostreams::regex_filter;

    fistream_.push(regex_filter{boost::regex{"\n"}, "\r\n"});
    fistream_.push(regex_filter{boost::regex{"\r"}, "\r\0"});
    fistream_.push(if_strm);
}
} // namespace tftp

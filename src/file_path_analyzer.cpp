/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "file_path_analyzer.hpp"
#include "error_code.hpp"     // for error_code
#include "tftp_exception.hpp" // for tftp_exception
#include <iostream>           // for basic_ostream, operator<<

namespace tftp {
file_path_analyzer::file_path_analyzer(boost::filesystem::path root_path) : root_path_{root_path} {
    std::cout << pth{root_path_}.make_preferred().string() << " " << root_path_.lexically_normal().string() << " "
              << root_path_.filename_is_dot() << std::endl;
}

boost::filesystem::path file_path_analyzer::check_and_produce_path(std::string file_path) const {
    pth incoming_path{file_path};
    pth dir_name_path = incoming_path.parent_path();
    pth file_name_path = incoming_path.filename();
    pth path = root_path_ / dir_name_path / file_name_path;
    path = path.lexically_normal();
    check_incoming_path_is_in_root_path(path);
    return path;
}

boost::filesystem::path file_path_analyzer::root_without_dot() const {
    auto p = pth{root_path_}.remove_filename();
    return p;
}

void file_path_analyzer::check_incoming_path_is_in_root_path(const pth& path) const {
    auto root_path = root_without_dot();
    auto root_it = root_path.begin();
    const auto root_end = root_path.end();
    auto path_it = path.begin();
    const auto path_end = path.end();
    while (root_it != root_end && path_it != path_end) {
        if (*root_it != *path_it) {
            throw tftp_exception(error_code::file_not_found, u8"Request for file out of working directory");
        }
        ++root_it;
        ++path_it;
    }
}

void file_path_analyzer::check_dir_part(const pth& path) const {
    if (!boost::filesystem::portable_directory_name(path.string())) {
        throw tftp_exception(error_code::file_not_found, u8"Directory path contains forbidden characters");
    }
}

void file_path_analyzer::check_file_part(const pth& path) const {
    if (!boost::filesystem::portable_file_name(path.string())) {
        throw tftp_exception(error_code::file_not_found, u8"File name contains forbidden characters.");
    }
}
} // namespace tftp

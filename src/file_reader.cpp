/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "file_reader.hpp"
#include <boost/filesystem/path.hpp> // for path
#include <gsl/gsl_util>              // for narrow

namespace tftp {
file_reader::file_reader(const boost::filesystem::path& path, std::ios_base::openmode mode)
    : if_strm{path, mode}, chunk_size_{message_traits<data_message>::data_payload_byte_size::def::value}, now_chunk_no_{
                                                                                                              0} {}

void file_reader::write_now_chunk(std::ostream& output) const {
    output.write(now_chunk_.data(), gsl::narrow<std::streamsize>(now_chunk_size_));
}

std::uint16_t file_reader::get_now_chunk_no() const { return now_chunk_no_; }

std::size_t file_reader::get_now_chunk_size() const { return now_chunk_size_; }

bool file_reader::is_now_chunk_last() const { return now_chunk_size_ < chunk_size_; }

void file_reader::read_next_chunk() {
    if (is_now_chunk_last()) {
        // TODO - throw error about end of file
    }
    read_next_chunk_impl();
    get_bytes_read();
    increment_chunk_count();
}
void file_reader::get_bytes_read() { now_chunk_size_ = get_bytes_read_impl(); }

void file_reader::increment_chunk_count() { now_chunk_no_++; }
} // namespace tftp

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ASCII_FILE_READER_HPP
#define ASCII_FILE_READER_HPP
#include "file_reader.hpp"                      // for file_reader
#include <boost/filesystem/path.hpp>            // for path
#include <boost/iostreams/filtering_stream.hpp> // for filtering_istream
#include <cstddef>                              // for size_t

namespace tftp {
class ascii_file_reader final : public file_reader {
  public:
    ascii_file_reader(const boost::filesystem::path& path);

  private:
    void read_next_chunk_impl() override;
    std::size_t get_bytes_read_impl() override;
    void prepare_filtering_stream();

    boost::iostreams::filtering_istream fistream_;
};
} // namespace tftp
#endif // ASCII_FILE_READER_HPP

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef FILE_PATH_ANALYZER_HPP
#define FILE_PATH_ANALYZER_HPP
#include <boost/filesystem/path.hpp> // for path
#include <string>                    // for string

namespace tftp {
class file_path_analyzer {
    using pth = boost::filesystem::path;

  public:
    file_path_analyzer(boost::filesystem::path root_path);
    boost::filesystem::path check_and_produce_path(std::string file_path) const;

  private:
    void check_incoming_path_is_in_root_path(const pth& path) const;
    void check_dir_part(const pth& path) const;
    void check_file_part(const pth& path) const;
    pth root_without_dot() const;

    const boost::filesystem::path root_path_;
};
} // namespace tftp
#endif // FILE_PATH_ANALYZER_HPP

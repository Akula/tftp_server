/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef FILE_READER_HPP
#define FILE_READER_HPP
#include "messages/messages_traits.hpp" // for message_traits
#include <array>                        // for array
#include <boost/filesystem/fstream.hpp> // for ifstream
#include <cstddef>                      // for size_t
#include <cstdint>                      // for uint16_t
#include <iostream>                     // for ios_base, ios_base::in, ios_...

namespace boost {
namespace filesystem {
class path;
} // namespace filesystem
} // namespace boost

namespace tftp {
class file_reader {
  public:
    file_reader(const boost::filesystem::path& path, std::ios_base::openmode mode = std::ios_base::in);
    void write_now_chunk(std::ostream& output) const;
    std::uint16_t get_now_chunk_no() const;
    std::size_t get_now_chunk_size() const;
    bool is_now_chunk_last() const;
    virtual ~file_reader() = default;
    void read_next_chunk();

  protected:
    std::array<char, message_traits<data_message>::data_payload_byte_size::def::value> now_chunk_;
    boost::filesystem::ifstream if_strm;
    const std::size_t chunk_size_;

  private:
    virtual void read_next_chunk_impl() = 0;
    virtual std::size_t get_bytes_read_impl() = 0;
    void increment_chunk_count();
    void get_bytes_read();

    std::uint16_t now_chunk_no_;
    std::size_t now_chunk_size_;
};
} // namespace tftp
#endif // FILE_READER_HPP

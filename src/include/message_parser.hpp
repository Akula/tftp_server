/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGE_PARSER_HPP
#define MESSAGE_PARSER_HPP
#include "network_order_stream_reader.hpp" // for network_order_stream_reader
#include "opcode.hpp"                      // for opcode
#include <cstddef>                         // for size_t
#include <iosfwd>                          // for istream forward declaration
#include <map>                             // for map
#include <memory>                          // for unique_ptr

namespace tftp {
class message;

class message_parser {
  public:
    message_parser(std::istream& stream);
    std::unique_ptr<message> parse(std::size_t raw_size);

  private:
    std::unique_ptr<message> parse_ack_message(std::size_t raw_size);
    std::unique_ptr<message> parse_read_message(std::size_t raw_size);
    std::unique_ptr<message> parse_write_message(std::size_t raw_size);
    std::unique_ptr<message> parse_error_message(std::size_t raw_size);
    std::unique_ptr<message> parse_data_message(std::size_t raw_size);
    std::unique_ptr<message> parse_option_ack_message(std::size_t raw_size);

    network_order_stream_reader reader_;
    using member_fun_parse_t = std::unique_ptr<message> (message_parser::*)(std::size_t);
    static std::map<opcode, member_fun_parse_t> mp;
    std::size_t last_msg_size_in_bytes_;
};
} // namespace tftp
#endif // MESSAGE_PARSER_HPP

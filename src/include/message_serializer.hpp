/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGE_SERIALIZER_HPP
#define MESSAGE_SERIALIZER_HPP
#include "error_code.hpp"                  // for error_code
#include "message_visitor.hpp"             // for message_visitor
#include "network_order_stream_writer.hpp" // for network_order_stream_writer
#include "opcode.hpp"                      // for opcode
#include <cstddef>                         // for size_t
#include <iosfwd>                          // for ostream
#include <memory>                          // for unique_ptr
#include <type_traits>                     // for underlying_type_t

namespace tftp {
class message;

class message_serializer : private message_visitor {
    using oc_ut = std::underlying_type_t<opcode>;
    using ec_ut = std::underlying_type_t<error_code>;

  public:
    message_serializer(std::ostream& stream);
    void serialize(std::unique_ptr<message> ptr);
    void serialize(const message& msg);
    std::size_t get_last_serialized_raw_size() const;

  private:
    void visit(const ack_message& msg) override;
    void visit(const read_request_message& msg) override;
    void visit(const write_request_message& msg) override;
    void visit(const error_message& msg) override;
    void visit(const data_message& msg) override;
    void visit(const opt_ack_message& msg) override;
    void advance_size(std::size_t size);
    std::size_t get_last_writen_bytes();

    network_order_stream_writer writer_;
    std::size_t last_serialized_raw_size_;
};
} // namespace tftp
#endif // MESSAGE_SERIALIZER_HPP

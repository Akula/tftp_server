/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGES_FILE_REQUEST_MESSAGE_HPP
#define MESSAGES_FILE_REQUEST_MESSAGE_HPP
#include "messages/message.hpp"
#include "opcode_fwd.hpp"        // for opcode forward declaration
#include "transfer_mode_fwd.hpp" // for transfer_mode forward declaration
#include <string>                // for string

namespace tftp {
class file_request_message : public message {
  public:
    file_request_message(const file_request_message& other);
    file_request_message& operator=(const file_request_message& other);
    file_request_message(file_request_message&& other) noexcept;
    file_request_message& operator=(file_request_message&& other) noexcept;
    ~file_request_message() override;
    std::string get_file_path() const;
    transfer_mode::mode get_transfer_mode() const;

  protected:
    file_request_message(std::string file_path, transfer_mode::mode transfer_mode);

  private:
    std::string file_path_;
    transfer_mode::mode transfer_mode_;
};
} // namespace tftp
#endif // MESSAGES_FILE_REQUEST_MESSAGE_HPP

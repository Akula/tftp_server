/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGES_MESSAGES_TRAITS_HPP
#define MESSAGES_MESSAGES_TRAITS_HPP
#include "ack_message.hpp"           // for ack_message
#include "data_message.hpp"          // for data_message
#include "error_code.hpp"            // for error_code
#include "error_message.hpp"         // for error_message
#include "message.hpp"               // for message
#include "opt_ack_message.hpp"       // for opt_ack_message
#include "read_request_message.hpp"  // for read_request_message
#include "write_request_message.hpp" // for write_request_message
#include <cstddef>                   // for size_t
#include <type_traits>               // for is_base_of_t, enable_if_t

namespace tftp {
template <typename T, T v1, T v2, T v3> struct sizes {
    using min = std::integral_constant<T, v1>;
    using def = std::integral_constant<T, v2>;
    using max = std::integral_constant<T, v3>;
};

template <typename raw_sizes> struct common_sizes { using raw_byte_size = raw_sizes; };

template <typename T, typename U = std::enable_if_t<std::is_base_of<message, T>::value>> struct message_traits {};
template <> struct message_traits<message> : public common_sizes<sizes<std::size_t, 4, 4, 656467>> {};
template <> struct message_traits<file_request_message> : public common_sizes<sizes<std::size_t, 6, 6, 512>> {};
template <> struct message_traits<read_request_message> : public message_traits<file_request_message> {};
template <> struct message_traits<write_request_message> : public message_traits<file_request_message> {};
template <> struct message_traits<ack_message> : public common_sizes<sizes<std::size_t, 4, 4, 4>> {};
template <> struct message_traits<data_message> : public common_sizes<sizes<std::size_t, 4, 516, 656467>> {
    using data_payload_byte_size = sizes<std::size_t, 0, 512, 656463>;
};
template <> struct message_traits<error_message> : public common_sizes<sizes<std::size_t, 6, 6, 512>> {};
template <> struct message_traits<opt_ack_message> : public common_sizes<sizes<std::size_t, 4, 4, 512>> {};
} // namespace tftp
#endif // MESSAGES_MESSAGES_TRAITS_HPP

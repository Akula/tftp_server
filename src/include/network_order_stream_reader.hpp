#ifndef NETWORK_ORDER_STREAM_READER_HPP
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define NETWORK_ORDER_STREAM_READER_HPP
#include <boost/endian/conversion.hpp> // for big_to_native, big_to_native_inplace
#include <cstddef>                     // for size_t
#include <istream>                     // for istream, basic_istream::read
#include <string>                      // for string
#include <type_traits>                 // for remove_cv_t, enable_if_t
#include <vector>                      // for vector

namespace tftp {
class network_order_stream_reader {
  public:
    network_order_stream_reader(std::istream& stream);
    template <typename T, typename = std::enable_if_t<std::is_integral<T>::value>> std::remove_cv_t<T> read() {
        T val{0};
        void* addr = &val;
        istream_.read(static_cast<char*>(addr), sizeof(val));
        check_stream(sizeof(val));
        if (sizeof(T) > 1) {
            return boost::endian::big_to_native(val);
        } else {
            return val;
        }
    }

    template <typename T, typename = std::enable_if_t<std::is_integral<T>::value>>
    std::vector<std::remove_cv_t<T>> read_n(std::size_t count) {
        std::vector<std::remove_cv_t<T>> vals;
        vals.reserve(count);
        void* addr = vals.data();
        istream_.read(static_cast<char*>(addr), count * sizeof(T));
        check_stream(count * sizeof(T));
        if (sizeof(T) > 1) {
            for (auto& val : vals)
                boost::endian::big_to_native_inplace(val);
        }
        return vals;
    }

    std::string read_string();

  private:
    void check_stream(std::size_t expected_bytes_read);
    void check_last_read_equals_expected(std::size_t expected_bytes_read);
    void check_eof();

    std::istream& istream_;
};
} // namespace tftp
#endif // NETWORK_ORDER_STREAM_READER_HPP

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NETWORK_ORDER_STREAM_WRITER_HPP
#define NETWORK_ORDER_STREAM_WRITER_HPP
#include <boost/endian/conversion.hpp> // for native_to_big_inplace
#include <cstddef>                     // for size_t
#include <gsl/span>                    // for span
#include <ostream>                     // for ostream, basic_ostream::write
#include <string>                      // for string
#include <type_traits>                 // for remove_cv_t, enable_if_t
#include <vector>                      // for vector

namespace tftp {
class network_order_stream_writer {
  public:
    network_order_stream_writer(std::ostream& stream);

    template <typename T, typename = std::enable_if_t<std::is_integral<T>::value>> void write(T v) {
        std::remove_cv_t<T> val{v};
        if (sizeof(T) > 1) {
            boost::endian::native_to_big_inplace(val);
        }

        void* addr = &val;
        stream_.write(static_cast<char*>(addr), sizeof(val));
        last_writed_size_ = sizeof(val);
        check_stream();
    }

    template <typename T, typename = std::enable_if_t<std::is_integral<T>::value>>
    void write_n(gsl::span<const T> data) {
        std::vector<std::remove_cv_t<T>> vals{data.cbegin(), data.cend()};

        if (sizeof(T) > 1) {
            for (auto& val : vals)
                boost::endian::native_to_big_inplace(val);
        }
        void* addr = vals.data();
        stream_.write(static_cast<char*>(addr), vals.size() * sizeof(T));
        last_writed_size_ = vals.size() * sizeof(T);
        check_stream();
    }

    void write_string(const std::string& s);
    std::size_t get_last_writed_bytes_count() const;

  private:
    void check_stream() const;

    std::ostream& stream_;
    std::size_t last_writed_size_;
};
} // namespace tftp
#endif // NETWORK_ORDER_STREAM_WRITER_HPP

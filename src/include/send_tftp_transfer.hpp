/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SEND_TFTP_TRANSFER_HPP
#define SEND_TFTP_TRANSFER_HPP
#include "ascii_file_reader.hpp"                  // for ascii_file_reader
#include "octet_file_reader.hpp"                  // for octet_file_reader
#include "tftp_transfer.hpp"                      // for tftp_transfer
#include <bits/shared_ptr.h>                      // for weak_ptr
#include <boost/filesystem/path.hpp>              // for path
#include <boost/iostreams/detail/wrap_unwrap.hpp> // for wrap
#include <boost/iostreams/traits.hpp>             // for direct_streambuf<>...
#include <cstdint>                                // for uint16_t
#include <memory>                                 // for make_unique
#include <vector>                                 // for vector

namespace tftp {
class tftp_connection;
class ack_message;

template <typename File_Reader> class send_tftp_transfer : public tftp_transfer {
  public:
    send_tftp_transfer(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint& remote_endpoint,
                       const boost::filesystem::path& path);
    void start_transfer() override;

    void visit(const write_request_message& msg) override;
    void visit(const read_request_message& msg) override;
    void visit(const error_message& msg) override;
    void visit(const data_message& msg) override;
    void visit(const ack_message& msg) override;
    void visit(const opt_ack_message& msg) override;

  private:
    void acknowledge_by_block_no(std::uint16_t block_no);
    std::vector<char> prepare_data_chunk();
    void send_data_msg();
    File_Reader file_reader_;
};
using ascii_send_tftp_transfer = send_tftp_transfer<ascii_file_reader>;
using octet_send_tftp_transfer = send_tftp_transfer<octet_file_reader>;
} // namespace tftp
#endif // SEND_TFTP_TRANSFER_HPP

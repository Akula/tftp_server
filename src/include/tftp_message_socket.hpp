/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef TFTP_MESSAGE_SOCKET_HPP
#define TFTP_MESSAGE_SOCKET_HPP

#include "message_parser.hpp"           // for message_parser
#include "message_serializer.hpp"       // for message_serializer
#include "messages/message.hpp"         // for message
#include "messages/messages_traits.hpp" // for message_trait
#include "tftp_exception.hpp"           // for tftp_exception
#include <boost/asio/buffer.hpp>        // for mutable_buffer
#include <boost/asio/ip/udp.hpp>        // for udp, udp::endpoint, udp::socket
#include <boost/asio/steady_timer.hpp>  // for steady_timer
#include <boost/asio/strand.hpp>        // for strand
#include <boost/asio/streambuf.hpp>     // for streambuf
#include <cstddef>                      // for size_t
#include <cstdint>                      // for uint16_t
#include <exception>                    // for exception_ptr, current_exception
#include <memory>                       // for unique_ptr, enable_shared_from_this, shared_ptr, weak_ptr
namespace boost {
namespace asio {
class io_service;
} // namespace asio
} // namespace boost

namespace tftp {
class tftp_message_socket {
  public:
    using msg_handler_t = std::function<void(std::unique_ptr<message>)>;
    using err_handler_t = std::function<void(std::exception_ptr)>;

    tftp_message_socket(boost::asio::io_service& io_service);
    tftp_message_socket(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint& local_endpoint);
    tftp_message_socket(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint& local_endpoint,
                        const boost::asio::ip::udp::endpoint& remote_endpoint);

    tftp_message_socket(const tftp_message_socket& other) = delete;
    tftp_message_socket& operator=(const tftp_message_socket& other) = delete;
    tftp_message_socket(tftp_message_socket&& other) noexcept = delete;
    tftp_message_socket& operator=(tftp_message_socket&& other) noexcept = delete;

    void async_send(const message& msg, err_handler_t err_handler);
    void async_send_to(const message& msg, const boost::asio::ip::udp::endpoint& remote_endpoint,
                       err_handler_t err_handler);
    void async_receive(msg_handler_t msg_handler, err_handler_t err_handler);
    void async_receive_from(boost::asio::ip::udp::endpoint& remote_endpoint, msg_handler_t msg_handler,
                            err_handler_t err_handler);
    void recieved_handler(const msg_handler_t& msg_handler, const err_handler_t& err_handler,
                          const boost::system::error_code& ec, std::size_t bytes_recvd);
    void bind(const boost::asio::ip::udp::endpoint& local_endpoint);
    void bind(const boost::asio::ip::udp::endpoint& local_endpoint, boost::system::error_code& ec);
    void close();
    void close(boost::system::error_code& ec);
    void cancel();
    void cancel(boost::system::error_code& ec);
    void connect(const boost::asio::ip::udp::endpoint& remote_endpoint);
    void connect(const boost::asio::ip::udp::endpoint& remote_endpoint, boost::system::error_code& ec);
    boost::asio::io_service& get_io_service();
    void shutdown(boost::asio::socket_base::shutdown_type type);
    void shutdown(boost::asio::socket_base::shutdown_type type, boost::system::error_code& ec);
    boost::asio::ip::udp::endpoint local_endpoint();
    boost::asio::ip::udp::endpoint local_endpoint(boost::system::error_code& ec);
    boost::asio::ip::udp::endpoint remote_endpoint();
    boost::asio::ip::udp::endpoint remote_endpoint(boost::system::error_code& ec);

  private:
    std::unique_ptr<message> parse_msg(std::size_t raw_size);
    std::size_t serialize_msg(const message& msg);

    boost::asio::ip::udp::socket socket_;
    boost::asio::streambuf incoming_data_buff_;
    boost::asio::streambuf outgoing_data_buff_;
};
} // namespace tftp

#endif // TFTP_MESSAGE_SOCKET_HPP

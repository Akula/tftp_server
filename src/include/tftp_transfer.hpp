/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef TFTP_TRANSFER_HPP
#define TFTP_TRANSFER_HPP
#include "message_visitor.hpp"
#include "tftp_message_socket.hpp"
#include <memory> // for unique_ptr, shared_ptr

namespace tftp {
class tftp_transfer : public message_visitor, public std::enable_shared_from_this<tftp_transfer> {
  public:
    tftp_transfer(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint& remote_endpoint);
    void on_incoming_message(const message& msg);
    virtual void start_transfer() = 0;
    tftp_transfer(const tftp_transfer& other) = delete;
    tftp_transfer& operator=(const tftp_transfer& other) = delete;
    tftp_transfer(tftp_transfer&& other) = default;
    tftp_transfer& operator=(tftp_transfer&& other) = default;
    ~tftp_transfer() override = default;
    void do_recieve_msg();

  protected:
    void send_msg(const message& msg);
    void end_transfer();
    tftp_message_socket socket_;

  private:
    void error_handler(std::exception_ptr ex_ptr);
    void handle_tftp_exception(const tftp_exception& ex);
};
} // namespace tftp
#endif // TFTP_TRANSFER_HPP

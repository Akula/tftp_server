#ifndef TRANSFER_MODE_FWD_HPP
#define TRANSFER_MODE_FWD_HPP
#include <cstdint>

namespace tftp {
namespace transfer_mode {
enum class mode : std::uint8_t;
} // namespace transfer_mode
} // namespace tftp

#endif // TRANSFER_MODE_FWD_HPP

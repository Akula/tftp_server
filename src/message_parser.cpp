/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "message_parser.hpp"
#include "error_code.hpp"                     // for error_code
#include "messages/ack_message.hpp"           // for ack_message
#include "messages/data_message.hpp"          // for data_message
#include "messages/error_message.hpp"         // for error_message
#include "messages/opt_ack_message.hpp"       // for opt_ack_message
#include "messages/read_request_message.hpp"  // for read_request_message
#include "messages/write_request_message.hpp" // for write_request_message
#include "tftp_exception.hpp"                 // for tftp_exception
#include "transfer_mode.hpp"                  // for transfer_mode transfer_mode::from_string
#include <cstdint>                            // for uint16_t
#include <ios>                                // for ios::failure
#include <stdexcept>                          // for out_of_range

namespace tftp {
message_parser::message_parser(std::istream& stream) : reader_{stream}, last_msg_size_in_bytes_{0} {}

std::unique_ptr<message> message_parser::parse(std::size_t raw_size) {
    last_msg_size_in_bytes_ = 0;
    try {
        auto oc = opcode(reader_.read<std::uint16_t>());
        return (this->*(mp[oc]))(raw_size - sizeof(std::uint16_t));
    } catch (std::out_of_range& /*ex*/) {
        throw tftp_exception(error_code::illegal_tftp_operation, u8"Unknown operation code.");
    } catch (std::ios::failure& ex) {
        throw tftp_exception(error_code::illegal_tftp_operation,
                             std::string{ex.what()} += std::string{u8" "} += ex.code().message());
    }
}

std::unique_ptr<message> message_parser::parse_ack_message(std::size_t raw_size) {
    auto block_no = reader_.read<std::uint16_t>();
    return std::make_unique<ack_message>(block_no);
}
std::unique_ptr<message> message_parser::parse_read_message(std::size_t raw_size) {
    std::string file_path = reader_.read_string();
    std::string mode_string = reader_.read_string();
    transfer_mode::mode m = transfer_mode::from_string(mode_string);
    return std::make_unique<read_request_message>(file_path, m);
}
std::unique_ptr<message> message_parser::parse_write_message(std::size_t raw_size) {
    std::string file_path = reader_.read_string();
    transfer_mode::mode m = transfer_mode::from_string(reader_.read_string());
    return std::make_unique<write_request_message>(file_path, m);
}
std::unique_ptr<message> message_parser::parse_error_message(std::size_t raw_size) {
    auto ec = error_code(reader_.read<std::uint16_t>());
    std::string msg = reader_.read_string();
    return std::make_unique<error_message>(ec, msg);
}
std::unique_ptr<message> message_parser::parse_data_message(std::size_t raw_size) {
    auto block_no = reader_.read<std::uint16_t>();
    std::vector<char> data = reader_.read_n<char>(raw_size - sizeof(std::uint16_t));
    return std::make_unique<data_message>(block_no, data);
}
std::unique_ptr<message> message_parser::parse_option_ack_message(std::size_t raw_size) {
    // TODO - implement parsing options ack message and parsing options in another fun
}

std::map<opcode, message_parser::member_fun_parse_t> message_parser::mp = {
    {opcode::acknowledgment, &message_parser::parse_ack_message},
    {opcode::read_request, &message_parser::parse_read_message},
    {opcode::write_request, &message_parser::parse_write_message},
    {opcode::error, &message_parser::parse_error_message},
    {opcode::data, &message_parser::parse_data_message},
    {opcode::option_acknowledgment, &message_parser::parse_option_ack_message}};
} // namespace tftp

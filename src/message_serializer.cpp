/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "message_serializer.hpp"
#include "messages/ack_message.hpp"           // for ack_message
#include "messages/data_message.hpp"          // for data_message
#include "messages/error_message.hpp"         // for error_message
#include "messages/message.hpp"               // for message
#include "messages/opt_ack_message.hpp"       // for opt_ack_message
#include "messages/read_request_message.hpp"  // for read_request_message
#include "messages/write_request_message.hpp" // for write_request_message
#include "tftp_exception.hpp"                 // for tftp_exception
#include "transfer_mode.hpp"                  // for mode_to_string
#include <cstddef>                            // for size_t
#include <cstdint>                            // for uint16_t
#include <gsl/span>                           // for span
#include <iostream>                           // for ios_base::failure, size_t, ios, ostream
#include <memory>                             // for unique_ptr
#include <stdexcept>                          // for out_of_range
#include <string>                             // for string
#include <vector>                             // for vector

namespace tftp {
message_serializer::message_serializer(std::ostream& stream) : writer_{stream}, last_serialized_raw_size_{0} {}

void message_serializer::serialize(std::unique_ptr<message> ptr) { serialize(*ptr); }

void message_serializer::serialize(const message& msg) {
    last_serialized_raw_size_ = 0;
    try {
        msg.accept_message_visitor(*this);
    } catch (std::ios::failure& ex) {
        throw tftp_exception(error_code::illegal_tftp_operation,
                             std::string{ex.what()} += std::string{" "} += ex.code().message());
    }
}
std::size_t message_serializer::get_last_serialized_raw_size() const { return last_serialized_raw_size_; }

void message_serializer::visit(const ack_message& msg) {
    writer_.write(static_cast<oc_ut>(opcode::acknowledgment));
    writer_.write(msg.get_block_no());
    advance_size(sizeof(oc_ut) + sizeof(decltype(std::declval<ack_message>().get_block_no())));
}
void message_serializer::visit(const read_request_message& msg) {
    writer_.write(static_cast<oc_ut>(opcode::read_request));
    writer_.write_string(transfer_mode::to_string(msg.get_transfer_mode()));
    advance_size(sizeof(oc_ut) + get_last_writen_bytes());
    writer_.write_string(msg.get_file_path());
    advance_size(get_last_writen_bytes());
}
void message_serializer::visit(const write_request_message& msg) {
    writer_.write(static_cast<oc_ut>(opcode::write_request));
    writer_.write_string(transfer_mode::to_string(msg.get_transfer_mode()));
    advance_size(sizeof(oc_ut) + get_last_writen_bytes());
    writer_.write_string(msg.get_file_path());
    advance_size(get_last_writen_bytes());
}
void message_serializer::visit(const error_message& msg) {
    writer_.write(static_cast<oc_ut>(opcode::error));
    writer_.write(static_cast<ec_ut>(msg.get_error_code()));
    writer_.write_string(msg.get_error_message());
    advance_size(sizeof(oc_ut) + sizeof(ec_ut) + get_last_writen_bytes());
}
void message_serializer::visit(const data_message& msg) {
    writer_.write(static_cast<oc_ut>(opcode::data));
    writer_.write(msg.get_block_no());
    writer_.write_n(gsl::span<const char>{msg.get_data()});
    advance_size(sizeof(oc_ut) + sizeof(decltype(std::declval<data_message>().get_block_no())) +
                 get_last_writen_bytes());
}
void message_serializer::visit(const opt_ack_message& msg) {
    // TODO - implement parsing options ack message and parsing options in another fun
}
void message_serializer::advance_size(std::size_t size) { last_serialized_raw_size_ += size; }

std::size_t message_serializer::get_last_writen_bytes() { return writer_.get_last_writed_bytes_count(); }
} // namespace tftp

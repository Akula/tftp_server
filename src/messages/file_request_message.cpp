/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "messages/file_request_message.hpp"
#include <utility> // for move

namespace tftp {

file_request_message::file_request_message(const file_request_message& other) = default;
file_request_message& file_request_message::operator=(const file_request_message& other) = default;
file_request_message::file_request_message(file_request_message&& other) noexcept = default;
file_request_message& file_request_message::operator=(file_request_message&& other) noexcept = default;
file_request_message::~file_request_message() = default;

std::string file_request_message::get_file_path() const { return file_path_; }

transfer_mode::mode file_request_message::get_transfer_mode() const { return transfer_mode_; }

file_request_message::file_request_message(std::string file_path, transfer_mode::mode transfer_mode)
    : file_path_{std::move(file_path)}, transfer_mode_{transfer_mode} {}
} // namespace tftp

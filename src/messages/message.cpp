#include "messages/message.hpp"

namespace tftp {
message::message() = default;
message::message(const message& other) = default;
message& message::operator=(const message& other) = default;
message::message(message&& other) noexcept = default;
message& message::operator=(message&& other) noexcept = default;
message::~message() = default;
} // namespace tftp

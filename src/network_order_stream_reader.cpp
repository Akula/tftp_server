/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "network_order_stream_reader.hpp"
#include <gsl/gsl_util> // for narrow

namespace tftp {
network_order_stream_reader::network_order_stream_reader(std::istream& stream) : istream_{stream} {}

std::string network_order_stream_reader::read_string() {
    std::string s{""};
    std::getline(istream_, s, '\0');
    check_eof();
    return s;
}

void network_order_stream_reader::check_stream(std::size_t expected_bytes_read) {
    check_eof();
    check_last_read_equals_expected(expected_bytes_read);
}

void network_order_stream_reader::check_last_read_equals_expected(std::size_t expected_bytes_read) {
    if (gsl::narrow<std::size_t>(istream_.gcount()) != expected_bytes_read) {
        throw std::ios::failure(u8"There is not enough data in stream to complete read operation.");
    }
}

void network_order_stream_reader::check_eof() {
    if (!istream_ && !istream_.eof()) {
        throw std::ios::failure(u8"Error while reading from input stream.");
    }
}
} // namespace tftp

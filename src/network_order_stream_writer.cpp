/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "network_order_stream_writer.hpp"
#include <cassert>      // for assert
#include <gsl/gsl_util> // for narrow
#include <limits>       // for numeric_limits

namespace tftp {
network_order_stream_writer::network_order_stream_writer(std::ostream& stream)
    : stream_{stream}, last_writed_size_{0} {}

void network_order_stream_writer::write_string(const std::string& s) {
    stream_.write(s.data(), gsl::narrow<std::streamsize>(s.size()));
    stream_.write("\0", 1);
    last_writed_size_ = s.size() + 1;
    check_stream();
}

std::size_t network_order_stream_writer::get_last_writed_bytes_count() const { return last_writed_size_; }

void network_order_stream_writer::check_stream() const {
    if (!stream_) {
        throw std::ios::failure(u8"Error when writing data into stream.");
    }
}
} // namespace tftp

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "octet_file_reader.hpp"
#include <array>                        // for array
#include <boost/filesystem/fstream.hpp> // for ifstream, binary
#include <cstddef>                      // for size_t
#include <gsl/gsl_util>                 // for narrow

namespace tftp {

octet_file_reader::octet_file_reader(const boost::filesystem::path& path)
    : file_reader{path, boost::filesystem::fstream::binary} {}

void octet_file_reader::read_next_chunk_impl() {
    if_strm.read(now_chunk_.data(), gsl::narrow<std::streamsize>(chunk_size_));
}

std::size_t octet_file_reader::get_bytes_read_impl() { return gsl::narrow<std::size_t>(if_strm.gcount()); }
} // namespace tftp

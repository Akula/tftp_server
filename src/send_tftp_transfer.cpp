/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "send_tftp_transfer.hpp"
#include <boost/iostreams/device/array.hpp> // for array_sink
#include <boost/iostreams/stream.hpp>       // for stream
#include <gsl/gsl_util>                     // for narrow
#include <utility>                          // for move

namespace tftp {

template <typename File_Reader>
send_tftp_transfer<File_Reader>::send_tftp_transfer(boost::asio::io_service& io_service,
                                                    const boost::asio::ip::udp::endpoint& remote_endpoint,
                                                    const boost::filesystem::path& path)
    : tftp_transfer{io_service, remote_endpoint}, file_reader_{path} {}

template <typename File_Reader> void send_tftp_transfer<File_Reader>::start_transfer() {
    file_reader_.read_next_chunk();
    send_data_msg();
    do_recieve_msg();
}

template <typename File_Reader> void send_tftp_transfer<File_Reader>::visit(const write_request_message& /*msg*/) {
    send_msg(error_message{error_code::illegal_tftp_operation,
                           u8"Incoming write request message is not allowed during file download."});
    end_transfer();
}
template <typename File_Reader> void send_tftp_transfer<File_Reader>::visit(const read_request_message& /*msg*/) {
    send_msg(error_message{error_code::illegal_tftp_operation,
                           u8"Incoming read request message is not allowed during file download."});
    end_transfer();
}
template <typename File_Reader> void send_tftp_transfer<File_Reader>::visit(const error_message& msg) {
    auto from = socket_.remote_endpoint();
    std::clog << "Incoming error message from: " << from.address().to_string() << ":" << from.port()
              << ", ec: " << static_cast<std::underlying_type_t<error_code>>(msg.get_error_code())
              << " reason: " << msg.get_error_message() << "\n";
    end_transfer();
}
template <typename File_Reader> void send_tftp_transfer<File_Reader>::visit(const data_message& /*msg*/) {
    send_msg(error_message{error_code::illegal_tftp_operation,
                           u8"Incoming data message is not allowed during file download."});
    end_transfer();
}
template <typename File_Reader> void send_tftp_transfer<File_Reader>::visit(const ack_message& msg) {
    auto block_no = msg.get_block_no();
    acknowledge_by_block_no(block_no);
}
template <typename File_Reader> void send_tftp_transfer<File_Reader>::visit(const opt_ack_message& /*msg*/) {
    send_msg(
        error_message{error_code::illegal_tftp_operation, u8"Option ack message is not allowed during file download."});
    end_transfer();
}

template <typename File_Reader> void send_tftp_transfer<File_Reader>::acknowledge_by_block_no(std::uint16_t block_no) {
    if (block_no == file_reader_.get_now_chunk_no()) {
        if (file_reader_.is_now_chunk_last()) {
            end_transfer();
        } else {
            file_reader_.read_next_chunk();
            send_data_msg();
        }
    }
}

template <typename File_Reader> void send_tftp_transfer<File_Reader>::send_data_msg() {
    auto data_chunk = prepare_data_chunk();
    std::unique_ptr<message> msg_ptr{std::make_unique<data_message>(
        gsl::narrow<std::uint16_t>(file_reader_.get_now_chunk_no()), std::move(data_chunk))};
    send_msg(*msg_ptr);
}

template <typename File_Reader> std::vector<char> send_tftp_transfer<File_Reader>::prepare_data_chunk() {
    std::vector<char> data;
    auto chunk_size = gsl::narrow<std::size_t>(file_reader_.get_now_chunk_size());
    data.resize(chunk_size);
    boost::iostreams::array_sink sink{data.data(), chunk_size};
    boost::iostreams::stream<boost::iostreams::array_sink> stream{sink};
    file_reader_.write_now_chunk(stream);
    return data;
}

template class send_tftp_transfer<ascii_file_reader>;
template class send_tftp_transfer<octet_file_reader>;
} // namespace tftp

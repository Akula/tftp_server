/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "server_options.hpp"
#include <stdexcept> // for runtime_error

namespace tftp {
server_options::server_options(bool allowed_to_read, bool allowed_to_write, bool allowed_to_create_subdirs,
                               std::uint16_t port, std::string root_path)
    : allowed_to_read_{allowed_to_read}, allowed_to_write_{allowed_to_write},
      allowed_to_create_subdirs_{allowed_to_create_subdirs}, port_{port}, root_path_{root_path} {
    check_write_and_catalog_perms();
}
bool server_options::is_allowed_to_read() const { return allowed_to_read_; }
bool server_options::is_allowed_to_write() const { return allowed_to_write_; }
bool server_options::is_allowed_to_create_subdirs() const { return allowed_to_create_subdirs_; }
std::uint16_t server_options::get_port() const { return port_; }
std::string server_options::get_root_path() const { return root_path_; }
void server_options::check_write_and_catalog_perms() {
    if (!allowed_to_write_ && allowed_to_create_subdirs_) {
        throw std::runtime_error(u8"Creating folders is not allowed without write permission.");
    }
}
} // namespace tftp

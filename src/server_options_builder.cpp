/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "server_options_builder.hpp"
#include "server_options.hpp" // for server_options

namespace tftp {
server_options_builder::server_options_builder()
    : allowed_to_read_{server_options::default_allowed_to_read},
      allowed_to_write_{server_options::default_allowed_to_write},
      allowed_to_create_subdirs_{server_options::default_allowed_to_create_subdirs},
      port_{server_options::default_port} {}
server_options_builder& server_options_builder::allowed_to_read(bool allowed) {
    allowed_to_read_ = allowed;
    return *this;
}

server_options_builder& server_options_builder::allowed_to_write(bool allowed) {
    allowed_to_write_ = allowed;
    return *this;
}

server_options_builder& server_options_builder::allowed_to_create_subdirs(bool allowed) {
    allowed_to_create_subdirs_ = allowed;
    return *this;
}

server_options_builder& server_options_builder::set_root_path(std::string path) {
    root_path_ = path;
    return *this;
}

server_options_builder& server_options_builder::set_port(std::uint16_t port) {
    port_ = port;
    return *this;
}

server_options server_options_builder::build() {
    return server_options(allowed_to_read_, allowed_to_write_, allowed_to_create_subdirs_, port_, root_path_);
}
} // namespace tftp

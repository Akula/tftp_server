/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "tftp_message_socket.hpp"

namespace tftp {

tftp_message_socket::tftp_message_socket(boost::asio::io_service& io_service)
    : socket_{io_service}, incoming_data_buff_{3 * message_traits<message>::raw_byte_size::max::value},
      outgoing_data_buff_{3 * message_traits<message>::raw_byte_size::max::value} {}

tftp_message_socket::tftp_message_socket(boost::asio::io_service& io_service,
                                         const boost::asio::ip::udp::endpoint& local_endpoint)
    : socket_{io_service, local_endpoint}, incoming_data_buff_{3 * message_traits<message>::raw_byte_size::max::value},
      outgoing_data_buff_{3 * message_traits<message>::raw_byte_size::max::value} {}

tftp_message_socket::tftp_message_socket(boost::asio::io_service& io_service,
                                         const boost::asio::ip::udp::endpoint& local_endpoint,
                                         const boost::asio::ip::udp::endpoint& remote_endpoint)
    : socket_{io_service, local_endpoint}, incoming_data_buff_{3 * message_traits<message>::raw_byte_size::max::value},
      outgoing_data_buff_{3 * message_traits<message>::raw_byte_size::max::value} {
    socket_.connect(remote_endpoint);
}

void tftp_message_socket::async_send(const message& msg, err_handler_t err_handler) {
    async_send_to(msg, socket_.remote_endpoint(), std::move(err_handler));
}
void tftp_message_socket::async_send_to(const message& msg, const boost::asio::ip::udp::endpoint& remote_endpoint,
                                        err_handler_t err_handler) {
    auto size = serialize_msg(msg);
    auto send_buffer = boost::asio::buffer(outgoing_data_buff_.data(), size);
    socket_.async_send_to(send_buffer, remote_endpoint,
                          [ this, size, err_handler = std::move(err_handler) ](const boost::system::error_code& ec,
                                                                               std::size_t /*bytes_transferred*/) {
                              try {
                                  outgoing_data_buff_.consume(size);
                                  if (ec) {
                                      throw std::system_error(ec.value(), std::system_category());
                                  }
                              } catch (...) {
                                  err_handler(std::current_exception());
                              }

                          });
}

void tftp_message_socket::async_receive(msg_handler_t msg_handler, err_handler_t err_handler) {
    socket_.async_receive(incoming_data_buff_.prepare(message_traits<message>::raw_byte_size::max::value),
                          [ this, msg_handler = std::move(msg_handler), err_handler = std::move(err_handler) ](
                              const boost::system::error_code& ec, std::size_t bytes_recvd) {
                              recieved_handler(msg_handler, err_handler, ec, bytes_recvd);
                          });
}
void tftp_message_socket::async_receive_from(boost::asio::ip::udp::endpoint& remote_endpoint, msg_handler_t msg_handler,
                                             err_handler_t err_handler) {
    socket_.async_receive_from(incoming_data_buff_.prepare(message_traits<message>::raw_byte_size::max::value),
                               remote_endpoint,
                               [ this, msg_handler = std::move(msg_handler), err_handler = std::move(err_handler) ](
                                   const boost::system::error_code& ec, std::size_t bytes_recvd) {
                                   recieved_handler(msg_handler, err_handler, ec, bytes_recvd);
                               });
}

void tftp_message_socket::recieved_handler(const msg_handler_t& msg_handler, const err_handler_t& err_handler,
                                           const boost::system::error_code& ec, std::size_t bytes_recvd) {
    try {
        incoming_data_buff_.commit(bytes_recvd);
        if (!ec) {
            auto msg = parse_msg(bytes_recvd);
            msg_handler(std::move(msg));
        } else {
            throw std::system_error(ec.value(), std::system_category());
        }
    } catch (...) {
        err_handler(std::current_exception());
    }
}

void tftp_message_socket::bind(const boost::asio::ip::udp::endpoint& local_endpoint) { socket_.bind(local_endpoint); }
void tftp_message_socket::bind(const boost::asio::ip::udp::endpoint& local_endpoint, boost::system::error_code& ec) {
    socket_.bind(local_endpoint, ec);
}
void tftp_message_socket::close() { socket_.close(); }
void tftp_message_socket::close(boost::system::error_code& ec) { socket_.close(ec); }
void tftp_message_socket::cancel() { socket_.cancel(); }
void tftp_message_socket::cancel(boost::system::error_code& ec) { socket_.cancel(ec); }
void tftp_message_socket::connect(const boost::asio::ip::udp::endpoint& remote_endpoint) {
    socket_.connect(remote_endpoint);
}
void tftp_message_socket::connect(const boost::asio::ip::udp::endpoint& remote_endpoint,
                                  boost::system::error_code& ec) {
    socket_.connect(remote_endpoint, ec);
}
boost::asio::io_service& tftp_message_socket::get_io_service() { return socket_.get_io_service(); }
void tftp_message_socket::shutdown(boost::asio::socket_base::shutdown_type type) { socket_.shutdown(type); }
void tftp_message_socket::shutdown(boost::asio::socket_base::shutdown_type type, boost::system::error_code& ec) {
    socket_.shutdown(type, ec);
}
boost::asio::ip::udp::endpoint tftp_message_socket::local_endpoint() { return socket_.local_endpoint(); }
boost::asio::ip::udp::endpoint tftp_message_socket::local_endpoint(boost::system::error_code& ec) {
    return socket_.local_endpoint(ec);
}
boost::asio::ip::udp::endpoint tftp_message_socket::remote_endpoint() { return socket_.remote_endpoint(); }
boost::asio::ip::udp::endpoint tftp_message_socket::remote_endpoint(boost::system::error_code& ec) {
    return socket_.remote_endpoint(ec);
}

std::unique_ptr<message> tftp_message_socket::parse_msg(std::size_t raw_size) {
    std::istream input_stream{&incoming_data_buff_};
    message_parser parser{input_stream};
    return parser.parse(raw_size);
}

std::size_t tftp_message_socket::serialize_msg(const message& msg) {
    std::ostream ostream{&outgoing_data_buff_};
    message_serializer serializer{ostream};
    serializer.serialize(msg);
    return serializer.get_last_serialized_raw_size();
}

} // namespace tftp

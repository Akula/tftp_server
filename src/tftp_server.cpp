/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "tftp_server.hpp"
#include "file_path_analyzer.hpp"      // for file_path_analyzer
#include "root_path_checker.hpp"       // for root_path_checker
#include "send_tftp_transfer.hpp"      // for octet{ascii}_send_transfer
#include "server_options.hpp"          // for server_options
#include "tftp_message_socket.hpp"     // for tftp_message_socket
#include "tftp_transfer.hpp"           // for tftp_transfer
#include "transfer_mode.hpp"           // for transfer_mode
#include <boost/asio/io_service.hpp>   // for io_service
#include <boost/asio/ip/address.hpp>   // for address
#include <boost/asio/ip/udp.hpp>       // for udp::socket, udp, udp::endpoint
#include <boost/asio/signal_set.hpp>   // for signal_set
#include <boost/asio/strand.hpp>       // for strand
#include <boost/system/error_code.hpp> // for error_code
#include <csignal>                     // for SIGINT, SIGQUIT, SIGTERM
#include <cstring>                     // for strsignal
#include <functional>                  // for bind
#include <iostream>                    // for operator<<, ostream, clog
#include <memory>                      // for make_shared, shared_ptr
#include <string>                      // for operator<<
#include <utility>                     // for move

namespace tftp {
class tftp_server::tftp_server_impl : public message_visitor {
  public:
    tftp_server_impl(server_options options);
    void start_server();

  private:
    void handle_stop_server();
    void connect_signals();
    void do_recieve_msg();
    void do_await_stop_by_signal();
    void send_msg_to_last_connected_no_callback(const message& msg);
    void handle_tftp_exception(const tftp_exception& ex);
    void check_server_reading_allowed() const;
    void check_server_writing_allowed() const;
    void check_server_creating_subdirs_allowed() const;

    void visit(const write_request_message& msg) override;
    void visit(const read_request_message& msg) override;
    void visit(const error_message& msg) override;
    void visit(const data_message& msg) override;
    void visit(const ack_message& msg) override;
    void visit(const opt_ack_message& msg) override;

  private:
    boost::asio::io_service io_service_;
    boost::asio::strand strand_;
    server_options options_;
    tftp_message_socket socket_;
    boost::asio::signal_set signal_set_;
    boost::asio::ip::udp::endpoint last_connected_;
};

tftp_server::tftp_server(server_options options) : pimpl_{std::make_unique<tftp_server_impl>(std::move(options))} {}

void tftp_server::start_server() { pimpl_->start_server(); }

tftp_server::~tftp_server() = default;
tftp_server::tftp_server(tftp_server&& server) noexcept = default;
tftp_server& tftp_server::operator=(tftp_server&& server) noexcept = default;

tftp_server::tftp_server_impl::tftp_server_impl(server_options options)
    : io_service_{}, strand_{io_service_}, options_{std::move(options)},
      socket_{io_service_, boost::asio::ip::udp::endpoint{boost::asio::ip::udp::v4(), options_.get_port()}},
      signal_set_{io_service_} {}

void tftp_server::tftp_server_impl::visit(const write_request_message& msg) {
    send_msg_to_last_connected_no_callback(
        error_message{error_code::illegal_tftp_operation, std::string{u8"Writting not supported."}});
}

void tftp_server::tftp_server_impl::send_msg_to_last_connected_no_callback(const message& msg) {
    socket_.async_send_to(msg, last_connected_, [](std::exception_ptr ex_ptr) {

    });
}

void tftp_server::tftp_server_impl::visit(const read_request_message& msg) {
    file_path_analyzer analyzer(boost::filesystem::path{options_.get_root_path()});
    try {
        check_server_reading_allowed();
        auto path = analyzer.check_and_produce_path(msg.get_file_path());
        {
            auto mode = msg.get_transfer_mode();
            if (mode == transfer_mode::mode::octet) {
                auto pointer = std::make_shared<octet_send_tftp_transfer>(io_service_, last_connected_, path);
                pointer->start_transfer();
            } else if (mode == transfer_mode::mode::netascii) {
                auto pointer = std::make_shared<ascii_send_tftp_transfer>(io_service_, last_connected_, path);
                pointer->start_transfer();
            } else {
                throw tftp_exception{error_code::illegal_tftp_operation, std::string{u8"Writing not allowed."}};
            }
        }
    } catch (const tftp_exception& ex) {
        handle_tftp_exception(ex);
    }
}
void tftp_server::tftp_server_impl::visit(const error_message& msg) {}
void tftp_server::tftp_server_impl::visit(const data_message& msg) {}
void tftp_server::tftp_server_impl::visit(const ack_message& msg) {}
void tftp_server::tftp_server_impl::visit(const opt_ack_message& msg) {}

void tftp_server::tftp_server_impl::check_server_reading_allowed() const {
    if (!options_.is_allowed_to_read())
        throw tftp_exception(error_code::illegal_tftp_operation, u8"Reading not allowed.");
}
void tftp_server::tftp_server_impl::check_server_writing_allowed() const {
    if (!options_.is_allowed_to_write())
        throw tftp_exception(error_code::illegal_tftp_operation, u8"Writing not allowed.");
}

void tftp_server::tftp_server_impl::check_server_creating_subdirs_allowed() const {
    if (!options_.is_allowed_to_create_subdirs())
        throw tftp_exception(error_code::illegal_tftp_operation, u8"Only writing to existing dirs is allowed.");
}

void tftp_server::tftp_server_impl::handle_tftp_exception(const tftp_exception& ex) {
    send_msg_to_last_connected_no_callback(error_message{ex.get_error_code(), ex.get_reason()});
}

void tftp_server::tftp_server_impl::start_server() {
    root_path_checker{}.check_root_path(options_.get_root_path());
    connect_signals();
    do_await_stop_by_signal();
    do_recieve_msg();
    io_service_.run();
}

void tftp_server::tftp_server_impl::do_recieve_msg() {
    socket_.async_receive_from(last_connected_,
                               [this](std::unique_ptr<message> msg) {
                                   msg->accept_message_visitor(*this);
                                   do_recieve_msg();
                               },
                               [this](std::exception_ptr ex_ptr) {
                                   try {
                                       if (ex_ptr)
                                           std::rethrow_exception(ex_ptr);
                                   } catch (std::system_error& ex) {
                                       std::clog << ex.code().value() << " " << ex.code().message() << " " << ex.what()
                                                 << "\n";
                                   } catch (tftp_exception& ex) {
                                       handle_tftp_exception(ex);
                                   }
                               });
}

void tftp_server::tftp_server_impl::handle_stop_server() {
    std::clog << "Stopping server...\n";
    socket_.close();
    io_service_.stop();
    std::clog << "Server stopped.\n";
}

void tftp_server::tftp_server_impl::connect_signals() {
    std::clog << "Connecting signals...\n";
    signal_set_.add(SIGTERM);
    signal_set_.add(SIGINT);
    signal_set_.add(SIGQUIT);
    std::clog << "Signals connected.\n";
}

void tftp_server::tftp_server_impl::do_await_stop_by_signal() {
    std::clog << "Posting signal await handler.\n";
    signal_set_.async_wait([this](const boost::system::error_code& ec, const int signal_number) {
        if (!ec) {
            std::clog << R"(")" << ::strsignal(signal_number) << R"(" signal handled.\n)";
            handle_stop_server();
        } else {
            std::cerr << "Error in signal received handler: " << ec.message() << "\n";
        }
    });
}
} // namespace tftp

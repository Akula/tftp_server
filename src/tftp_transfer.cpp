/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "tftp_transfer.hpp"
#include "messages/ack_message.hpp" // for ack_message
#include "messages/message.hpp"     // for message
#include "opcode.hpp"               // for opcode
#include <exception>                // for exception_ptr, rethrow_exception
#include <iostream>                 // for clog
#include <stdexcept>                // for runtime_error
#include <utility>                  // for move

namespace tftp {

tftp_transfer::tftp_transfer(boost::asio::io_service& io_service, const boost::asio::ip::udp::endpoint& remote_endpoint)
    : socket_{io_service, boost::asio::ip::udp::endpoint{boost::asio::ip::udp::v4(), 0}, remote_endpoint} {}

void tftp_transfer::do_recieve_msg() {
    auto self{shared_from_this()};
    socket_.async_receive(
        [self, this](std::unique_ptr<message> msg) {
            do_recieve_msg();
            on_incoming_message(*msg);
        },
        [self, this](std::exception_ptr ex_ptr) { error_handler(ex_ptr); });
} // namespace tftp

void tftp_transfer::end_transfer() {
    socket_.shutdown(boost::asio::socket_base::shutdown_both);
    socket_.close();
}

void tftp_transfer::send_msg(const message& msg) {
    auto self = shared_from_this();
    socket_.async_send(msg, [self, this](std::exception_ptr ex_ptr) { error_handler(ex_ptr); });
}

void tftp_transfer::error_handler(std::exception_ptr ex_ptr) {
    try {
        if (ex_ptr)
            std::rethrow_exception(ex_ptr);
    } catch (std::system_error& ex) {
        std::clog << ex.code().value() << " " << ex.code().message() << " " << ex.what() << "\n";
    } catch (tftp_exception& ex) {
        handle_tftp_exception(ex);
    }
}

void tftp_transfer::handle_tftp_exception(const tftp_exception& ex) {
    socket_.async_send(error_message{ex.get_error_code(), ex.get_reason()}, [](std::exception_ptr) {});
}

void tftp_transfer::on_incoming_message(const message& msg) { msg.accept_message_visitor(*this); }
}

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "transfer_mode.hpp"
#include <algorithm> // for to_lower
#include <sstream>   // for stringstream

namespace tftp {
namespace transfer_mode {
mode from_string(std::string mode_string) {
    std::transform(mode_string.begin(), mode_string.end(), mode_string.begin(), ::tolower);
    mode m = mode::unsupported;
    if (mode_string == "octet" || mode_string == "binary")
        m = mode::octet;
    else if (mode_string == "netascii")
        m = mode::netascii;
    else if (mode_string == "mail")
        m = mode::mail;
    return m;
}

std::string to_string(mode m) {
    std::stringstream s;

    if (m == mode::mail) {
        s << "mail";
    } else if (m == mode::netascii) {
        s << "netascii";
    } else if (m == mode::octet) {
        s << "octet";
    } else if (m == mode::unsupported) {
        s << "unsupported";
    } else {
        s << std::to_string(std::underlying_type_t<mode>(m));
    }

    return s.str();
}
} // namespace transfer_mode
} // namespace tftp

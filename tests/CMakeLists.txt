# The MIT License (MIT)
#
# Copyright (c) 2017 Artur Kula
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

cmake_minimum_required(VERSION 3.4)
project(tftp_server_tests LANGUAGES CXX)

### BEGIN EXTERNAL PART ###
# part copied from https://github.com/google/googletest/tree/master/googletest#incorporating-into-an-existing-cmake-project
# for googletest license and copyright see THIRD_PARTY_INFO in tftp_server project root folder

# Download and unpack googletest at configure time
configure_file(cmake/gtest_CMakeLists.txt.in ${CMAKE_BINARY_DIR}/googletest-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_BINARY_DIR}/googletest-src
                 ${CMAKE_BINARY_DIR}/googletest-build)

# The gtest/gtest_main targets carry header search path
# dependencies automatically when using CMake 2.8.11 or
# later. Otherwise we have to add them here ourselves.
if (CMAKE_VERSION VERSION_LESS 2.8.11)
  include_directories("${gtest_SOURCE_DIR}/include")
endif()
### END EXTERNAL PART ###

macro(my_add_test test_name test_source_file)
    add_executable(${test_name} ${test_source_file} ${HEADER_FILES})
    target_include_directories(
        ${test_name}
        PRIVATE
        ${CMAKE_SOURCE_DIR}/tests/src/include
        ${TFTP_SERVER_ALL_INCLUDE_DIRS})
    target_link_libraries(${test_name} tftp_server gmock gtest)
    add_test(NAME ${test_name} COMMAND ${test_name})
endmacro()

my_add_test(ack_message_tests src/messages/ack_message_tests.cpp)
my_add_test(error_message_tests src/messages/error_message_tests.cpp)
my_add_test(read_request_message_tests src/messages/read_request_message_tests.cpp)
my_add_test(write_request_message_tests src/messages/write_request_message_tests.cpp)
my_add_test(data_message_tests src/messages/data_message_tests.cpp)
my_add_test(transfer_mode_tests src/transfer_mode_tests.cpp)

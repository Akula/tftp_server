/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGE_VISITOR_MOCK_HPP
#define MESSAGE_VISITOR_MOCK_HPP

#include "message_visitor.hpp"
#include "messages/ack_message.hpp"
#include "messages/data_message.hpp"
#include "messages/error_message.hpp"
#include "messages/opt_ack_message.hpp"
#include "messages/read_request_message.hpp"
#include "messages/write_request_message.hpp"
#include <gmock/gmock.h>

namespace tftp {
namespace test {

class message_visitor_mock : public message_visitor {
  public:
    MOCK_METHOD1(visit, void(const write_request_message& msg));
    MOCK_METHOD1(visit, void(const read_request_message& msg));
    MOCK_METHOD1(visit, void(const error_message& msg));
    MOCK_METHOD1(visit, void(const data_message& msg));
    MOCK_METHOD1(visit, void(const ack_message& msg));
    MOCK_METHOD1(visit, void(const opt_ack_message& msg));
};

} // namespace test
} // namespace tftp

#endif // MESSAGE_VISITOR_MOCK_HPP

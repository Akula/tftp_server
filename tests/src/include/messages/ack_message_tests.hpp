/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGES_ACK_MESSAGE_TESTS_HPP
#define MESSAGES_ACK_MESSAGE_TESTS_HPP

#include "messages/ack_message.hpp"
#include <gtest/gtest.h>

namespace tftp {
namespace test {
class ack_message_tests : public testing::Test {
  protected:
    static constexpr std::uint16_t default_block_no = 10;
    static constexpr std::uint16_t temporary_block_no = 20;
    static constexpr std::uint16_t fixture_msg_constructor_block_no = 30;
    static ack_message get_temporary_ack_msg() { return ack_message{temporary_block_no}; }

    ack_message fixture_msg{fixture_msg_constructor_block_no};
};
} // namespace test
} // namespace tftp

#endif

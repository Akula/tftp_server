/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGES_DATA_MESSAGE_TESTS_HPP
#define MESSAGES_DATA_MESSAGE_TESTS_HPP

#include "messages/data_message.hpp"
#include <gtest/gtest.h>

namespace tftp {
namespace test {
class data_message_tests : public testing::Test {
  public:
    data_message_tests() : testing::Test(), fixture_msg{fixture_block_no, get_fixture_data()} {}

  protected:
    static constexpr std::uint16_t fixture_block_no = 10;
    static constexpr std::uint16_t default_block_no = 20;
    static constexpr std::uint16_t temporary_block_no = 30;
    static data_message get_temporary_data_msg() { return data_message{temporary_block_no, get_temporary_data()}; }
    static std::vector<char> get_temporary_data() { return {0x1C, 0x24, 0x2A, 0x57, 0x3B, 0x61, 0x09, 0x6E}; }
    static std::vector<char> get_default_data() { return {0x01, 0x15, 0x61, 0x20, 0x43, 0x38, 0x0F, 0x7D}; }
    static std::vector<char> get_fixture_data() { return {0x75, 0x49, 0x2E, 0x00, 0x33, 0x73, 0x16, 0x18}; }
    data_message fixture_msg;
};
} // namespace test
} // namespace tftp

#endif // MESSAGES_DATA_MESSAGE_TESTS_HPP

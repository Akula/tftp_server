/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGES_ERROR_MESSAGE_TESTS_HPP
#define MESSAGES_ERROR_MESSAGE_TESTS_HPP

#include "messages/error_message.hpp"
#include <gtest/gtest.h>

namespace tftp {
namespace test {
class error_message_tests : public testing::Test {
  protected:
    static constexpr error_code default_error_code = error_code::file_not_found;
    static constexpr char default_error_msg_text[] = "def_err_msg";
    static constexpr error_code fixture_error_code = error_code::file_already_exists;
    static constexpr char fixture_error_msg_text[] = "fixture_err_msg";
    static constexpr error_code temporary_error_code = error_code::illegal_tftp_operation;
    static constexpr char temporary_error_msg_text[] = "temporary_err_msg";
    static error_message get_temporary_ack_msg() {
        return error_message(temporary_error_code, std::string{temporary_error_msg_text});
    }
    static std::vector<error_code> get_vector_with_all_error_codes() {
        return {error_code::access_violation, error_code::disk_full_or_allocation_exceeded,
                error_code::file_not_found,   error_code::illegal_tftp_operation,
                error_code::no_such_user,     error_code::not_defined,
                error_code::option_error,     error_code::unknown_transfer_id};
    }

    error_message fixture_msg{fixture_error_code, std::string{fixture_error_msg_text}};
};
} // namespace test
} // namespace tftp

#endif

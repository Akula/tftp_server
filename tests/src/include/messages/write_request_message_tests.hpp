/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MESSAGES_WRITE_REQUEST_MESSAGE_TESTS_HPP
#define MESSAGES_WRITE_REQUEST_MESSAGE_TESTS_HPP

#include "messages/write_request_message.hpp"
#include "transfer_mode.hpp"
#include <gtest/gtest.h>

namespace tftp {
namespace test {
class write_request_message_tests : public testing::Test {
  protected:
    static constexpr transfer_mode::mode default_transfer_mode = transfer_mode::mode::mail;
    static constexpr char default_file_path_text[] = "def_file_path";
    static constexpr transfer_mode::mode fixture_transfer_mode = transfer_mode::mode::octet;
    static constexpr char fixture_file_path_text[] = "fixture_file_path";
    static constexpr transfer_mode::mode temporary_transfer_mode = transfer_mode::mode::netascii;
    static constexpr char temporary_file_path_text[] = "temporary_file_path";
    static write_request_message get_temporary_write_request_msg() {
        return write_request_message{std::string{temporary_file_path_text}, temporary_transfer_mode};
    }
    static std::vector<transfer_mode::mode> get_vector_with_all_transfer_modes() {
        return {transfer_mode::mode::mail, transfer_mode::mode::netascii, transfer_mode::mode::octet,
                transfer_mode::mode::unsupported};
    }

    write_request_message fixture_msg{std::string{fixture_file_path_text}, fixture_transfer_mode};
};
} // namespace test
} // namespace tftp

#endif // MESSAGES_WRITE_REQUEST_MESSAGE_TESTS_HPP

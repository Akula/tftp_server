/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "messages/data_message_tests.hpp"
#include "message_visitor_mock.hpp"
#include "opcode.hpp"

namespace tftp {
namespace test {
constexpr std::uint16_t data_message_tests::default_block_no;
constexpr std::uint16_t data_message_tests::temporary_block_no;
constexpr std::uint16_t data_message_tests::fixture_block_no;

TEST_F(data_message_tests, data_message_opcode_is_right) {
    data_message msg{default_block_no, get_default_data()};
    EXPECT_EQ(msg.get_opcode(), opcode::data);
}

TEST_F(data_message_tests, data_message_created_with_block_no_return_same_block_no) {
    data_message msg{default_block_no, get_default_data()};
    EXPECT_EQ(msg.get_block_no(), default_block_no);
}

TEST_F(data_message_tests, data_message_created_with_minimal_limit_value_of_block_no_return_same_block_no) {
    auto min = std::numeric_limits<std::uint16_t>::min();
    data_message msg{min, get_default_data()};
    EXPECT_EQ(msg.get_block_no(), min);
    EXPECT_NE(msg.get_block_no(), min - 1);
    EXPECT_NE(msg.get_block_no(), min + 1);
}

TEST_F(data_message_tests, data_message_created_with_maximal_limit_value_of_block_no_return_same_block_no) {
    auto max = std::numeric_limits<std::uint16_t>::max();
    data_message msg{max, get_default_data()};
    EXPECT_EQ(msg.get_block_no(), max);
    EXPECT_NE(msg.get_block_no(), max - 1);
    EXPECT_NE(msg.get_block_no(), max + 1);
}

TEST_F(data_message_tests, data_message_created_with_data_return_equal_data) {
    data_message msg{default_block_no, get_default_data()};
    EXPECT_EQ(msg.get_data(), get_default_data());
}

TEST_F(data_message_tests, data_message_created_from_another_with_copy_constructor_return_equal_values) {
    data_message created_msg{fixture_msg};
    EXPECT_EQ(created_msg.get_block_no(), fixture_msg.get_block_no());
    EXPECT_EQ(created_msg.get_data(), fixture_msg.get_data());
}

TEST_F(data_message_tests, data_message_copied_from_another_with_copy_assignment_operator_return_equal_values) {
    data_message created_msg{default_block_no, get_default_data()};
    created_msg = fixture_msg;
    EXPECT_EQ(created_msg.get_block_no(), fixture_msg.get_block_no());
    EXPECT_NE(created_msg.get_block_no(), default_block_no);
    EXPECT_EQ(created_msg.get_data(), fixture_msg.get_data());
    EXPECT_EQ(created_msg.get_data(), get_default_data());
}

TEST_F(data_message_tests, data_message_move_created_from_another_with_move_constructor_return_equal_values) {
    data_message created_msg{get_temporary_data_msg()};
    EXPECT_EQ(created_msg.get_block_no(), temporary_block_no);
    EXPECT_EQ(created_msg.get_data(), get_temporary_data());
}

TEST_F(data_message_tests, data_message_moved_from_another_with_move_assignment_operator_return_equal_values) {
    data_message created_msg{default_block_no, get_default_data()};
    created_msg = get_temporary_data_msg();
    EXPECT_EQ(created_msg.get_block_no(), temporary_block_no);
    EXPECT_NE(created_msg.get_block_no(), default_block_no);
    EXPECT_EQ(created_msg.get_data(), get_temporary_data());
    EXPECT_NE(created_msg.get_data(), get_default_data());
}

TEST_F(data_message_tests, data_message_polymorphic_call_opcode_is_right) {
    message& msg = fixture_msg;
    EXPECT_EQ(msg.get_opcode(), opcode::data);
    EXPECT_EQ(msg.get_opcode(), fixture_msg.get_opcode());
}

TEST_F(data_message_tests, data_message_accept_visitor_calls_proper_visitor_overload) {
    using ::testing::_;
    using ::testing::Matcher;
    message_visitor_mock visitor;
    EXPECT_CALL(visitor, visit(Matcher<const data_message&>(_))).Times(1);
    fixture_msg.accept_message_visitor(visitor);
}

TEST_F(data_message_tests, data_message_polymorphic_accept_visitor_calls_proper_visitor_overload) {
    using ::testing::_;
    using ::testing::Matcher;
    message& msg = fixture_msg;
    message_visitor_mock visitor;
    EXPECT_CALL(visitor, visit(Matcher<const data_message&>(_))).Times(1);
    msg.accept_message_visitor(visitor);
}
} // namespace test
} // namespace tftp

int main(int argc, char* argv[]) {
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

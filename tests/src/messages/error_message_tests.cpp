/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "messages/error_message_tests.hpp"
#include "message_visitor_mock.hpp"
#include "opcode.hpp"

namespace tftp {
namespace test {
constexpr error_code error_message_tests::default_error_code;
constexpr char error_message_tests::default_error_msg_text[];
constexpr error_code error_message_tests::fixture_error_code;
constexpr char error_message_tests::fixture_error_msg_text[];
constexpr error_code error_message_tests::temporary_error_code;
constexpr char error_message_tests::temporary_error_msg_text[];

TEST_F(error_message_tests, error_message_opcode_is_right) {
    error_message msg{default_error_code, default_error_msg_text};
    EXPECT_EQ(msg.get_opcode(), opcode::error);
}

TEST_F(error_message_tests, error_message_created_with_all_error_codes_returns_same_code) {
    for (auto err_code : get_vector_with_all_error_codes()) {
        error_message msg{err_code, default_error_msg_text};
        EXPECT_EQ(err_code, msg.get_error_code());
    }
}

TEST_F(error_message_tests, error_message_created_with_error_message_text_returns_same_error_message_text) {
    for (const char* str : {"", "msg", "long long error message text"}) {
        error_message msg{default_error_code, str};
        EXPECT_STREQ(msg.get_error_message().c_str(), str);
    }
}

TEST_F(error_message_tests,
       error_message_created_from_another_with_copy_constructor_return_equal_error_code_and_error_message) {
    error_message created_msg{fixture_msg};
    EXPECT_EQ(created_msg.get_error_code(), fixture_msg.get_error_code());
    EXPECT_STREQ(created_msg.get_error_message().c_str(), fixture_msg.get_error_message().c_str());
}

TEST_F(error_message_tests,
       error_message_copied_from_another_with_copy_assignment_operator_return_equal_error_code_and_error_message) {
    error_message created_msg{default_error_code, default_error_msg_text};
    created_msg = fixture_msg;
    EXPECT_EQ(created_msg.get_error_code(), fixture_error_code);
    EXPECT_NE(created_msg.get_error_code(), default_error_code);
    EXPECT_STREQ(created_msg.get_error_message().c_str(), fixture_error_msg_text);
    EXPECT_STRNE(created_msg.get_error_message().c_str(), default_error_msg_text);
}

TEST_F(error_message_tests,
       error_message_move_created_from_another_with_move_constructor_return_equal_error_code_and_error_message) {
    error_message created_msg{get_temporary_ack_msg()};
    EXPECT_EQ(created_msg.get_error_code(), temporary_error_code);
    EXPECT_STREQ(created_msg.get_error_message().c_str(), temporary_error_msg_text);
}

TEST_F(error_message_tests,
       error_message_moved_from_another_with_move_assignment_operator_return_equal_error_code_and_error_message) {
    error_message created_msg{default_error_code, default_error_msg_text};
    created_msg = get_temporary_ack_msg();
    EXPECT_EQ(created_msg.get_error_code(), temporary_error_code);
    EXPECT_NE(created_msg.get_error_code(), default_error_code);
    EXPECT_STREQ(created_msg.get_error_message().c_str(), temporary_error_msg_text);
    EXPECT_STRNE(created_msg.get_error_message().c_str(), default_error_msg_text);
}

TEST_F(error_message_tests, error_message_polymorphic_call_opcode_is_right) {
    message& msg = fixture_msg;
    EXPECT_EQ(msg.get_opcode(), opcode::error);
    EXPECT_EQ(msg.get_opcode(), fixture_msg.get_opcode());
}

TEST_F(error_message_tests, error_message_accept_visitor_calls_proper_visitor_overload) {
    using ::testing::_;
    using ::testing::Matcher;
    message_visitor_mock visitor;
    EXPECT_CALL(visitor, visit(Matcher<const error_message&>(_))).Times(1);
    fixture_msg.accept_message_visitor(visitor);
}

TEST_F(error_message_tests, error_message_polymorphic_accept_visitor_calls_proper_visitor_overload) {
    using ::testing::_;
    using ::testing::Matcher;
    message& msg = fixture_msg;
    message_visitor_mock visitor;
    EXPECT_CALL(visitor, visit(Matcher<const error_message&>(_))).Times(1);
    msg.accept_message_visitor(visitor);
}
} // namespace test
} // namespace tftp

int main(int argc, char* argv[]) {
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

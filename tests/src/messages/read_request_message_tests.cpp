/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "messages/read_request_message_tests.hpp"
#include "message_visitor_mock.hpp"
#include "opcode.hpp"

namespace tftp {
namespace test {
constexpr transfer_mode::mode read_request_message_tests::default_transfer_mode;
constexpr char read_request_message_tests::default_file_path_text[];
constexpr transfer_mode::mode read_request_message_tests::fixture_transfer_mode;
constexpr char read_request_message_tests::fixture_file_path_text[];
constexpr transfer_mode::mode read_request_message_tests::temporary_transfer_mode;
constexpr char read_request_message_tests::temporary_file_path_text[];

TEST_F(read_request_message_tests, read_request_message_opcode_is_right) {
    read_request_message msg{std::string{default_file_path_text}, default_transfer_mode};
    EXPECT_EQ(msg.get_opcode(), opcode::read_request);
}

TEST_F(read_request_message_tests, read_request_message_created_with_all_transfer_modes_returns_same_mode) {
    for (auto transfer_mode : get_vector_with_all_transfer_modes()) {
        read_request_message msg{std::string{default_file_path_text}, transfer_mode};
        EXPECT_EQ(transfer_mode, msg.get_transfer_mode());
    }
}

TEST_F(read_request_message_tests, read_request_message_created_with_file_path_text_returns_same_file_path_text) {
    for (const char* str : {"", "path", "longer/path/to/../../the/file.txt", "long path string with spaces text"}) {
        read_request_message msg{std::string{str}, default_transfer_mode};
        EXPECT_STREQ(msg.get_file_path().c_str(), str);
    }
}

TEST_F(read_request_message_tests,
       read_request_message_created_from_another_with_copy_constructor_return_equal_transfer_mode_and_file_path_text) {
    read_request_message created_msg{fixture_msg};
    EXPECT_EQ(created_msg.get_transfer_mode(), fixture_msg.get_transfer_mode());
    EXPECT_STREQ(created_msg.get_file_path().c_str(), fixture_msg.get_file_path().c_str());
}

TEST_F(
    read_request_message_tests,
    read_request_message_copied_from_another_with_copy_assignment_operator_return_equal_transfer_mode_and_file_path_text) {
    read_request_message created_msg{std::string{default_file_path_text}, default_transfer_mode};
    created_msg = fixture_msg;
    EXPECT_EQ(created_msg.get_transfer_mode(), fixture_msg.get_transfer_mode());
    EXPECT_NE(created_msg.get_transfer_mode(), default_transfer_mode);
    EXPECT_STREQ(created_msg.get_file_path().c_str(), fixture_msg.get_file_path().c_str());
    EXPECT_STRNE(created_msg.get_file_path().c_str(), default_file_path_text);
}

TEST_F(
    read_request_message_tests,
    read_request_message_move_created_from_another_with_move_constructor_return_equal_transfer_mode_and_file_path_text) {
    read_request_message created_msg{get_temporary_read_request_msg()};
    EXPECT_EQ(created_msg.get_transfer_mode(), temporary_transfer_mode);
    EXPECT_STREQ(created_msg.get_file_path().c_str(), temporary_file_path_text);
}

TEST_F(
    read_request_message_tests,
    read_request_message_moved_from_another_with_move_assignment_operator_return_equal_transfer_mode_and_file_path_text) {
    read_request_message created_msg{std::string{default_file_path_text}, default_transfer_mode};
    created_msg = get_temporary_read_request_msg();
    EXPECT_EQ(created_msg.get_transfer_mode(), temporary_transfer_mode);
    EXPECT_NE(created_msg.get_transfer_mode(), default_transfer_mode);
    EXPECT_STREQ(created_msg.get_file_path().c_str(), temporary_file_path_text);
    EXPECT_STRNE(created_msg.get_file_path().c_str(), default_file_path_text);
}

TEST_F(read_request_message_tests, read_request_message_polymorphic_call_opcode_is_right) {
    message& msg = fixture_msg;
    EXPECT_EQ(msg.get_opcode(), opcode::read_request);
    EXPECT_EQ(msg.get_opcode(), fixture_msg.get_opcode());
    file_request_message& msg2 = fixture_msg;
    EXPECT_EQ(msg2.get_opcode(), opcode::read_request);
    EXPECT_EQ(msg2.get_opcode(), fixture_msg.get_opcode());
}

TEST_F(read_request_message_tests, read_request_message_polymorphic_call_transfer_mode_is_right) {
    file_request_message& msg = fixture_msg;
    EXPECT_EQ(msg.get_transfer_mode(), fixture_msg.get_transfer_mode());
}

TEST_F(read_request_message_tests, read_request_message_polymorphic_call_file_path_is_right) {
    file_request_message& msg = fixture_msg;
    EXPECT_STREQ(msg.get_file_path().c_str(), fixture_msg.get_file_path().c_str());
}

TEST_F(read_request_message_tests, read_request_message_accept_visitor_calls_proper_visitor_overload) {
    using ::testing::_;
    using ::testing::Matcher;
    message_visitor_mock visitor;
    EXPECT_CALL(visitor, visit(Matcher<const read_request_message&>(_))).Times(1);
    fixture_msg.accept_message_visitor(visitor);
}

TEST_F(read_request_message_tests, read_request_message_polymorphic_accept_visitor_calls_proper_visitor_overload) {
    using ::testing::_;
    using ::testing::Matcher;
    message& msg = fixture_msg;
    message_visitor_mock visitor;
    EXPECT_CALL(visitor, visit(Matcher<const read_request_message&>(_))).Times(1);
    msg.accept_message_visitor(visitor);
}
} // namespace test
} // namespace tftp

int main(int argc, char* argv[]) {
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

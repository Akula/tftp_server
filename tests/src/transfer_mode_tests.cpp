/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Artur Kula
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "transfer_mode.hpp"
#include <gtest/gtest.h>

namespace tftp {
namespace test {
TEST(transfer_mode_tests, to_string_return_proper_transfer_mode_string_for_each_transfer_mode) {
    ASSERT_STRCASEEQ(transfer_mode::to_string(transfer_mode::mode::mail).c_str(), "mail");
    ASSERT_STRCASEEQ(transfer_mode::to_string(transfer_mode::mode::netascii).c_str(), "netascii");
    ASSERT_STRCASEEQ(transfer_mode::to_string(transfer_mode::mode::octet).c_str(), "octet");
    ASSERT_STRCASEEQ(transfer_mode::to_string(transfer_mode::mode::unsupported).c_str(), "unsupported");
}

TEST(transfer_mode_test,
     to_string_return_string_represantation_of_value_used_to_create_mode_from_unsupported_values_of_underlying_type) {
    std::underlying_type_t<transfer_mode::mode> no_mode_value = 10;
    auto ret_str = transfer_mode::to_string(transfer_mode::mode(no_mode_value));
    ASSERT_STREQ(transfer_mode::to_string(transfer_mode::mode(no_mode_value)).c_str(),
                 std::to_string(no_mode_value).c_str());
}

TEST(transfer_mode_test, from_string_return_proper_mode_for_all_case_insensitive_input) {
    for (const char* cstr : {"mail", "MAIL", "Mail", "MAil", "MAIl", "MAiL", "MaIL"}) {
        ASSERT_EQ(transfer_mode::from_string(cstr), transfer_mode::mode::mail);
    }
    for (const char* cstr : {"binary", "BINAry", "Binary", "BINARY", "octet", "Octet", "OCTET", "OCTeT"}) {
        ASSERT_EQ(transfer_mode::from_string(cstr), transfer_mode::mode::octet);
    }
    for (const char* cstr : {"netascii", "Netascii", "NETASCII", "NeTaScII", "NETAScii"}) {
        ASSERT_EQ(transfer_mode::from_string(cstr), transfer_mode::mode::netascii);
    }
}

TEST(transfer_mode_test, from_string_return_unsupported_mode_for_wrong_input) {
    for (const char* cstr : {"mmail", "MAaIL", "Maiil", "MAik", "MSIl", "MAiiL", "MZIL"}) {
        auto mode = transfer_mode::from_string(cstr);
        ASSERT_NE(mode, transfer_mode::mode::mail);
        ASSERT_EQ(mode, transfer_mode::mode::unsupported);
    }
    for (const char* cstr : {"bbinary", "BIiNAry", "Binaryu", "BINARYy", "osctet", "Onctet", "OCTETT", "OCTseT"}) {
        auto mode = transfer_mode::from_string(cstr);
        ASSERT_NE(mode, transfer_mode::mode::octet);
        ASSERT_EQ(mode, transfer_mode::mode::unsupported);
    }
    for (const char* cstr : {"netasci", "Neetascii", "ASCII", "aScII", "NETASci", "asci"}) {
        auto mode = transfer_mode::from_string(cstr);
        ASSERT_NE(mode, transfer_mode::mode::netascii);
        ASSERT_EQ(mode, transfer_mode::mode::unsupported);
    }
    for (const char* cstr : {"wrong_mode", "bad_mode", "other_word", "badword", "not even one word"}) {
        ASSERT_EQ(transfer_mode::from_string(cstr), transfer_mode::mode::unsupported);
    }
}
} // namespace test
} // namespace tftp

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
